package uk.ac.ed.pguaglia.real.lang;

public class Difference extends BinaryOperation {

	public Difference ( Expression left, Expression right ) {
		super( left, right, Expression.Type.DIFFERENCE );
	}
}
