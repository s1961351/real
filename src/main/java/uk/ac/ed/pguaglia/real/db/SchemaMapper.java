package uk.ac.ed.pguaglia.real.db;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.RecognitionException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import uk.ac.ed.pguaglia.real.lang.Expression;
import uk.ac.ed.pguaglia.real.lang.ReplacementException;

@SuppressWarnings("serial")
public final class SchemaMapper extends ObjectMapper {

	// FIELD NAMES
	private static final String TABLES = "tables";
	private static final String TABLE_ATTRIBUTES = "attributes";
	private static final String ATTRIBUTE_TYPE = "type";
	private static final String TABLE_DATAFILE = "datafile";
	private static final String VIEWS = "views";

	private class SchemaSerializer extends StdSerializer<Schema> {

		public SchemaSerializer() {
			this(null);
		}

		public SchemaSerializer(Class<Schema> t) {
			super(t);
		}

		@Override
		public void serialize(Schema sch, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject(); // start schema
			gen.writeObjectFieldStart(TABLES); // start tables
			for (String tableName : sch.getTableNames()) {
				gen.writeObjectFieldStart(tableName); // start table
				gen.writeObjectFieldStart(TABLE_ATTRIBUTES); // start attributes
				for (String attrName : sch.getTableAttributes(tableName)) {
					gen.writeObjectFieldStart(attrName);
					gen.writeObjectField(ATTRIBUTE_TYPE,"STRING");
					gen.writeEndObject();
				}
				gen.writeEndObject(); // end attributes
				gen.writeObjectField(TABLE_DATAFILE, sch.getTableDatafile(tableName));
				gen.writeEndObject(); // end table
			}
			gen.writeEndObject(); //end tables
			gen.writeObjectFieldStart(VIEWS); // start views
			for (String viewName : sch.getViewNames()) {
				gen.writeObjectField(viewName, sch.getViewDefinition(viewName).toString());
			}
			gen.writeEndObject(); // end views
			gen.writeEndObject(); //end schema
		}
	}

	private class SchemaDeserializer extends StdDeserializer<Schema> {

		public SchemaDeserializer() { 
			this(null); 
		} 

		public SchemaDeserializer(Class<?> vc) { 
			super(vc); 
		}

		@Override
		public Schema deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
			JsonNode node = jp.getCodec().readTree(jp);

			JsonNode tables = node.get(TABLES);
			Iterator<String> tableNames = tables.fieldNames();
			Map<String,List<String>> tableMap = new HashMap<>();
			Map<String,File> dataMap = new HashMap<>();
			while (tableNames.hasNext()) {
				String name =  tableNames.next();
				JsonNode tbl = tables.get(name);
				List<String> attributes = new ArrayList<>();
				Iterator<String> attrNames = tbl.get(TABLE_ATTRIBUTES).fieldNames();
				while (attrNames.hasNext()) {
					attributes.add(attrNames.next());
				}
				tableMap.put(name, attributes);
				dataMap.put(name, new File(tbl.get(TABLE_DATAFILE).textValue()));
			}

			JsonNode views = node.get(VIEWS);
			Iterator<String> viewNames = views.fieldNames();
			Map<String,Expression> viewsMap = new HashMap<>();
			while (viewNames.hasNext()) {
				String name = viewNames.next();
				try {
					viewsMap.put(name, Expression.parse(views.get(name).textValue()));
				} catch (RecognitionException | ReplacementException e) {
					throw new JsonProcessingException(e) {};
				}
			}

			try {
				return new Schema(tableMap, dataMap, viewsMap);
			} catch (IllegalArgumentException e) {
				throw new JsonProcessingException(e) {};
			}
		}
	}

	private static final SchemaMapper INSTANCE = new SchemaMapper();

	private SchemaMapper() {
		super();
		SimpleModule schemaModule = 
				new SimpleModule("SchemaModule", new Version(1, 0, 0, null, null, null));
		schemaModule.addSerializer(Schema.class, new SchemaSerializer());
		schemaModule.addDeserializer(Schema.class, new SchemaDeserializer());
		this.registerModule(schemaModule);
	}

	public static final SchemaMapper getInstance() {
		return INSTANCE;
	}
}
