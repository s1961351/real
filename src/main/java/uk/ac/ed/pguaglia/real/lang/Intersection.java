package uk.ac.ed.pguaglia.real.lang;

public class Intersection extends BinaryOperation {

	public Intersection ( Expression left, Expression right ) {
		super( left, right, Expression.Type.INTERSECTION );
	}
}
