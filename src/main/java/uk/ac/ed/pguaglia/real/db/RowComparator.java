package uk.ac.ed.pguaglia.real.db;

import java.util.Comparator;

public class RowComparator implements Comparator<String[]> {

	@Override
	public int compare(String[] o1, String[] o2) {
		int len = o1.length <= o2.length ? o1.length : o2.length;
		for (int i=0; i < len; i++) {
			int cmp = o1[i].compareTo(o2[i]);
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
		}
		return o1.length <= o2.length ? 0 : -1;
	}
}
