package uk.ac.ed.pguaglia.real.lang;

import java.util.Map;

public class Term {

	private String value;
	private boolean constant;

	public Term ( String value, boolean constant ) {
		this.value = value;
		this.constant = constant;
	}

	public boolean isConstant() {
		return constant;
	}

	public boolean isAttribute() {
		return !constant;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return constant ? "'" + value + "'" : value;
	}

	public String getValue(String[] record, Map<String,Integer> attr) {
		return constant ? value : record[attr.get(value)];
	}
}
