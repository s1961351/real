package uk.ac.ed.pguaglia.real.lang;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.DatabaseException;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.Table;


public class Projection extends Expression {

	private Expression operand;
	private List<String> attributes;

	public Projection ( List<String> attributes, Expression operand ) {
		super(Expression.Type.PROJECTION);
		this.operand = operand;
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return String.format( "%s[%s]( %s )",
				this.getType().getConnective(),
				String.join(",", attributes),
				operand.toString() );
	}

	@Override
	public String toSyntaxTreeString(String prefix, Schema schema) {
		String tree = "%1$s%2$s\n%3$s |\n%3$s +--- %4$s";
		String conn = this.getType().getConnective();
		String attr = attributes.toString().replace(" ", "");
		String s;
		if ((operand.getType() == Expression.Type.BASE)
				&& ((schema == null) || ((BaseExpression) operand).isTable(schema))) {
			s = operand.toSyntaxTreeString("", schema);
			s += "\n" + prefix;
		} else {
			s = operand.toSyntaxTreeString(prefix + "      ", schema);
		}
		return String.format(tree, conn, attr, prefix, s);
	}

	@Override
	public Set<String> signature(Schema schema) throws SchemaException {
		Set<String> allAttributes = new HashSet<String>(operand.signature(schema));
		Set<String> projAttributes = new HashSet<String>(attributes);
		if (allAttributes.containsAll(projAttributes)) {
			return projAttributes;
		} else {
			projAttributes.removeAll(allAttributes);
			String attr = projAttributes.iterator().next();
			throw SchemaException.attributeNotFound(attr, this, schema);
		}
	}

	@Override
	public Table execute(Database db, boolean bags) throws DatabaseException {
		signature(db.schema());
		Table tbl = operand.execute(db,bags).project(new HashSet<>(attributes));
		return bags ? tbl : tbl.distinct();
	}

	@Override
	public Set<String> getBaseNames() {
		return operand.getBaseNames();
	}
}
