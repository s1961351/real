package uk.ac.ed.pguaglia.real.db;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import uk.ac.ed.pguaglia.real.lang.BaseExpression;
import uk.ac.ed.pguaglia.real.lang.Expression;

public class Schema {

	private Map<String,List<String>> tables;
	private Map<String,File> datafiles; 
	private Map<String,Expression> views;
	private Map<String,Set<String>> isUsedBy;

	public Schema() {
		tables = new HashMap<>();
		views = new HashMap<>();
		isUsedBy = new HashMap<>();
		datafiles = new HashMap<>();
	}

	public static Schema fromJSON(File src) throws JsonParseException, JsonMappingException, IOException {
		return SchemaMapper.getInstance().readValue(src, Schema.class);
	}

	protected Schema(Map<String, List<String>> tables, Map<String, File> datafiles, Map<String, Expression> views)
			throws IllegalArgumentException {
		Set<String> tableNames = tables.keySet();
		Set<String> viewNames = views.keySet();
		if (SetUtils.isEqualSet(tableNames, datafiles.keySet()) == false) {
			throw new IllegalArgumentException();
		}
		if (CollectionUtils.containsAny(tableNames, viewNames)) {
			throw new IllegalArgumentException();
		}
		Set<String> baseNames = SetUtils.union(tableNames, viewNames);

		this.tables = new HashMap<>(tables);
		this.datafiles = new HashMap<>(datafiles);
		this.views = new HashMap<>();
		this.isUsedBy = new HashMap<>();

		for (String name : views.keySet()) {
			Expression expr = views.get(name);
			if (baseNames.containsAll(expr.getBaseNames()) == false) {
				throw new IllegalArgumentException();
			}
			this.views.put(name, expr);
			updateDeps(name, expr);
		}
	}

	public File getTableDatafile(String name) throws IOException {
		if (datafiles.containsKey(name) == false) {
			String msg = String.format("ERROR: \"%s\" (No such table)", name);
			throw new IOException(msg);
		}
		return datafiles.get(name);
	}

	public boolean hasTable(String name) {
		return tables.containsKey(name);
	}

	public boolean hasView(String name) {
		return views.containsKey(name);
	}

	public List<String> getTableAttributes(String name) {
		return tables.get(name);
	}

	public Expression getViewDefinition(String name) {
		return views.get(name);
	}

	public void addTable(String name, List<String> attributes, File data) throws Exception {
		if (tables.containsKey(name)) {
			throw new Exception(String.format("ERROR: Table %s already exists", name));
		}
		tables.put(name, attributes);
		datafiles.put(name, data.getAbsoluteFile());
	}

	public List<String> addTable(String name, List<String> attributes) {
		return tables.put(name, attributes);
	}

	public Expression addView(String name, Expression def) throws SchemaException {
		if (views.containsKey(name)) {
			String msg = String.format("View \"%s\" already defined", name);
			throw new SchemaException(msg, def, this);
		}
		if (tables.containsKey(name)) {
			String msg = String.format("A table with name \"%s\" already exists", name);
			throw new SchemaException(msg, def, this);
		}
		views.put(name, def);
		updateDeps(name, def);
		return def;
	}

	public void drop(String name) throws SchemaException {
		if (isUsedBy.containsKey(name)) {
			String msg = String.format("Table or view \"%s\" is used by the following views: %s", name,
					String.join(", ", isUsedBy.get(name)));
			throw new SchemaException(msg, new BaseExpression(name), this);
		}
		if (tables.containsKey(name)) {
			tables.remove(name);
			datafiles.remove(name);
		} else if (views.containsKey(name)) {
			views.remove(name);
		} else {
			String msg = String.format("Table or view \"%s\" not found", name);
			throw new SchemaException(msg, new BaseExpression(name), this);
		}
	}

	private void updateDeps(String name, Expression expr) {
		for (String base : expr.getBaseNames()) {
			if (isUsedBy.containsKey(base)) {
				isUsedBy.get(base).add(name);
			} else {
				Set<String> deps = new HashSet<>();
				deps.add(name);
				isUsedBy.put(base, deps);
			}
		}
	}

	public Set<String> getViewNames() {
		return views.keySet();
	}

	public Set<String> getTableNames() {
		return tables.keySet();
	}

	public Set<String> getTableNames(Expression expr) throws SchemaException {
		Set<String> base = new HashSet<>();
		for (String name : expr.getBaseNames()) {
			if (this.hasView(name)) {
				base.addAll(this.getTableNames(this.getViewDefinition(name)));
			} else if (this.hasTable(name)) {
				base.add(name);
			} else {
				String msg = String.format("No such table \"%s\"", name);
				throw new SchemaException(msg, expr, this);
			}
		}
		return base;
	}
}
