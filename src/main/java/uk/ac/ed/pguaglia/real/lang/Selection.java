package uk.ac.ed.pguaglia.real.lang;

import java.util.Set;

import uk.ac.ed.pguaglia.real.Utils;
import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.DatabaseException;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.Table;

public class Selection extends Expression {

	private Expression expr;
	private Condition cond;

	public Selection ( Condition cond, Expression expr ) {
		super(Expression.Type.SELECTION);
		this.expr = expr;
		this.cond = cond;
	}

	@Override
	public String toString() {
		return String.format( "%s[%s]( %s )",
				this.getType().getConnective(),
				cond.toString(),
				expr.toString() );
	}

	@Override
	public String toSyntaxTreeString(String prefix, Schema schema) {
		String tree = "%1$s[%2$s]\n%3$s |\n%3$s +--- %4$s";
		String conn = this.getType().getConnective();
		String cond = this.cond.toString()//.replace(" ", "");
				.replace("( ", "(")
				.replace(" )", ")")
				.replace(" = ", "=");
		String s = Utils.toSyntaxTreeString(expr, schema, prefix, "      ");
		return String.format(tree, conn, cond, prefix, s);
	}

	@Override
	public Set<String> signature(Schema schema) throws SchemaException {
		Set<String> condAttrs = cond.signature();
		Set<String> exprAttrs = Utils.clone(expr.signature(schema));
		if (exprAttrs.containsAll(condAttrs)) {
			return exprAttrs;
		} else {
			condAttrs.removeAll(exprAttrs);
			String attr = condAttrs.iterator().next();
			throw SchemaException.attributeNotFound(attr, this, schema);
		}
	}

	@Override
	public Table execute(Database db, boolean bags) throws DatabaseException {
		signature(db.schema());
		return expr.execute(db,bags).select(cond);
	}

	@Override
	public Set<String> getBaseNames() {
		return expr.getBaseNames();
	}
}
