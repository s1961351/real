package uk.ac.ed.pguaglia.real.lang;

import java.util.Map;
import java.util.Set;

public class Negation extends Condition {

	private Condition cond;

	public Negation ( Condition c ) {
		super( Condition.Type.NEGATION );
		this.cond = c;
	}

	@Override
	public String toString() {
		return String.format("%s( %s )", getType().getConnective(), cond.toString());
	}

	@Override
	public Set<String> signature() {
		return cond.signature();
	}

	@Override
	public boolean satisfied(String[] record, Map<String, Integer> attr) {
		return !cond.satisfied(record, attr);
	}
}
