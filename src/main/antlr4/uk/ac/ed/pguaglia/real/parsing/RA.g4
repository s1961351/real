grammar RA;

fragment
DIGIT
:
	[0-9]
;

fragment
LETTER
:
	[A-Za-z]
;

NAME
:
	LETTER+
	(
		LETTER
		| DIGIT
		| '_'
	)*
;

CONSTANT
:
	'\'' .+? '\''
;

WHITESPACE
:
	[ \t\r\n]+ -> skip
;

PRODUCT
:
	'<X>'
;

UNION
:
	'<U>'
;

INTERSECTION
:
	'<I>'
;

DIFFERENCE
:
	'<D>'
;

AND
:
	'&'
;

OR
:
	'|'
;

NOT
:
	'~'
;

SELECTION
:
	'<S>'
;

PROJECTION
:
	'<P>'
;

RENAMING
:
	'<R>'
;

DISTINCT
:
	'<E>'
;

attribute
:
	NAME
;

attribute_list
:
	attribute
	(
		',' attribute
	)* # AttributeList
;

constant
:
	CONSTANT
;

term
:
	attribute
	| constant
;

replacement
:
	attribute '->' attribute
;

replacement_list
:
	replacement
	(
		',' replacement
	)* # ReplacementList
;

condition
:
	'(' condition ')' # ParenthesizedCondition
	| NOT condition # Negation
	| condition AND condition # Conjunction
	| condition OR condition # Disjunction
	| term '=' term # Equality
;

expression
:
	'(' expression ')' # ParenthesizedExpression
	| expression PRODUCT expression # Product
	| expression INTERSECTION expression # Intersection
	| expression UNION expression # Union
	| expression DIFFERENCE expression # Difference
	| PROJECTION '[' attribute_list ']' '(' expression ')' # Projection
	| SELECTION '[' condition ']' '(' expression ')' # Selection
	| RENAMING '[' replacement_list ']' '(' expression ')' # Renaming
	| DISTINCT '(' expression ')' # Distinct
	| NAME # BaseRelation
;

start
:
	| expression EOF
	;
